from .views import UserViewSet
from django.conf.urls import url


urlpatterns = [
    url(
        r'^users/(?P<pk>\d+)/$',
        UserViewSet.as_view({
            "get": "retrieve"}),
        name="user-detail"),
]
