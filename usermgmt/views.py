from rest_framework import viewsets, mixins
from .serializers import UserSerializer
from .models import User


class UserViewSet(
        mixins.RetrieveModelMixin,
        viewsets.GenericViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()
