from .views import AssessmentViewSet, \
    QuestionAttemptViewSet, AssessmentTemplateViewSet
from django.conf.urls import url
from rest_framework.routers import SimpleRouter


router = SimpleRouter()
router.register(r'assessments', AssessmentViewSet)
router.register(r'question-attempts', QuestionAttemptViewSet)
router.register(r'templates', AssessmentTemplateViewSet)


urlpatterns = router.urls
