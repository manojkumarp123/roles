from django.db import models
from usermgmt.models import User


class AssessmentTemplate(models.Model):
    """Generate an Assessment"""
    max_question_count = models.PositiveSmallIntegerField()
    author = models.ForeignKey(
        User, related_name="assessments_authored")
    title = models.CharField(max_length=200, unique=True)


class AbstractQuestion(models.Model):
    text = models.CharField(max_length=200)
    answer = models.CharField(max_length=200)

    class Meta:
        abstract = True


class Question(AbstractQuestion):
    """Author a Question"""
    author = models.ForeignKey(
        User, related_name="questions_authored")


class Assessment(models.Model):
    """Initialize an Attempt, Submit & Evaluate"""
    user = models.ForeignKey(User)
    completed = models.BooleanField(default=False)
    score = models.PositiveSmallIntegerField(
        null=True, blank=True)
    template_used = models.ForeignKey(
        AssessmentTemplate,
        related_name="assessments_generated")

    class Meta:
        """Allow one attempt per user"""
        unique_together = (
            "user", "template_used")

    def save(self, *args, **kwargs):
        if self.completed:
             self.score = self.question_attempts.filter(result=True).count()*10
        super(Assessment, self).save(*args, **kwargs)


class QuestionAttempt(AbstractQuestion):
    """Solve a question which is part of an Assessment"""
    user = models.ForeignKey(User)
    question_in_reference = models.ForeignKey(Question)
    user_response = models.CharField(
        max_length=200, null=True, blank=True)
    result = models.NullBooleanField()
    assessment = models.ForeignKey(
        Assessment, related_name="question_attempts")

    class Meta:
        unique_together = (
            "user", "question_in_reference", "assessment")

    def save(self, *args, **kwargs):
        self.result = self.user_response == self.answer
        super(QuestionAttempt, self).save(*args, **kwargs)
