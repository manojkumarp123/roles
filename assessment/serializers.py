from rest_framework import serializers
from .models import Assessment, QuestionAttempt, \
    AssessmentTemplate


class AssessmentTemplateSerializer(serializers.ModelSerializer):\

    class Meta:
        model = AssessmentTemplate
        fields = "__all__"


class QuestionAttemptSerializer(serializers.ModelSerializer):

    class Meta:
        model = QuestionAttempt
        fields = "__all__"
        read_only_fields = (
            "user", "question_in_reference", "result",
            "assessment", "text", "answer")


class AssessmentSerializer(serializers.ModelSerializer):
    question_attempts = QuestionAttemptSerializer(
        read_only=True, many=True)

    class Meta:
        model = Assessment
        fields = "__all__"
        read_only_fields = ("score",)
