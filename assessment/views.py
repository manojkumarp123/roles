from rest_framework import viewsets, mixins
from .serializers import AssessmentSerializer, \
    QuestionAttemptSerializer, AssessmentTemplateSerializer
from .models import Assessment, QuestionAttempt, \
    AssessmentTemplate


class AssessmentTemplateViewSet(
        mixins.ListModelMixin,
        viewsets.GenericViewSet):
    serializer_class = AssessmentTemplateSerializer
    queryset = AssessmentTemplate.objects.all()


class AssessmentViewSet(
        mixins.UpdateModelMixin,
        mixins.RetrieveModelMixin,
        viewsets.GenericViewSet):
    serializer_class = AssessmentSerializer
    queryset = Assessment.objects.all()


class QuestionAttemptViewSet(
        mixins.UpdateModelMixin,
        viewsets.GenericViewSet):
    serializer_class = QuestionAttemptSerializer
    queryset = QuestionAttempt.objects.all()
