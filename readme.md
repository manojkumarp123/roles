# Group Permissions

1. explicit roles

### Models

* Assessment Template

* Question (What happens when question is modified?)

* Assessment (questions stays unchanged)

* User

* AssessmentAttempt (merged into Assessment)

* QuestionAttempt


### Walkthrough


GET tests/
[1, 2, 3, 4]

POST assessments/ {test_template_id:1}
"test: 1 has begun"

GET assessments/1/
{question_attempts:[{id:1, text: "what?"}, {id:2, text: "why?"}, {id:3, text: "where?"}, {id:4, text: "How?"}]}

PATCH assessments/1/attempt-questions/1/ {response: 10units}
"response registered"

PATCH assessments/1/ {completed: True}
"You have completed your test"

GET assessments/1/
"Your score: 1"


### Setup


create assessment template instances
